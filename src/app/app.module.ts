import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { RecetaFormularioComponent } from './receta-formulario/receta-formulario.component';
import { ListaRecetasComponent } from './lista-recetas/lista-recetas.component';

@NgModule({
  declarations: [
    AppComponent,
    RecetaFormularioComponent,
    ListaRecetasComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
