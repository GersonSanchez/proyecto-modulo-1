export class PlatilloReceta {

    nombre_receta: string;
    ingredientes_receta: string[];
    procedimiento_receta: string;
    imagen_receta: string;

    constructor( nombre: string, ingredientes: string[], procedimiento: string, imagen: string ) {
        this.nombre_receta = nombre;
        this.ingredientes_receta = ingredientes;
        this.procedimiento_receta = procedimiento;
        this.imagen_receta = imagen;
    }

}