import { Component, Input, HostBinding } from '@angular/core';
import { PlatilloReceta } from './../models/platillo-receta.model';

@Component({
  selector: 'app-receta-formulario',
  templateUrl: './receta-formulario.component.html',
  styleUrls: ['./receta-formulario.component.css']
})
export class RecetaFormularioComponent {

  @Input() receta: PlatilloReceta;
  @HostBinding('attr.class') cssClass = 'row justify-content-md-center';

  constructor() { }

}
