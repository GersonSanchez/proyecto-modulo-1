import { Component } from '@angular/core';
import { PlatilloReceta } from './../models/platillo-receta.model';

@Component({
  selector: 'app-lista-recetas',
  templateUrl: './lista-recetas.component.html',
  styleUrls: ['./lista-recetas.component.css']
})
export class ListaRecetasComponent  {

  recetas: PlatilloReceta[];
  ingredientes: string[];

  constructor() {
    this.recetas = [];
    this.ingredientes = [];
  }

  agregar( ingrediente: string ) {

    this.ingredientes.push( ingrediente );
    console.log(this.ingredientes);
    
  }

  guardar(nombre: string, procedimiento: string, imagen: string) {

    this.recetas.push( new PlatilloReceta( nombre, this.ingredientes, procedimiento, imagen ) );
    this.ingredientes = [];
    return false;

  }

}
